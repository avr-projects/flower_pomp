#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define POMP 	(1<<PB4) // sterowanie pompy
#define SENS 	(1<<PB3) // czujnik poziomu wilgotno�ci
#define KEY1 	(1<<PB2) // przycisk do r�cznego uruchomienia pompy
#define CHECK 	(1<<PB1) // czujnik braku wody w zbiorniku
#define BUZZ 	(1<<PB0) // buzzer sygnalizuj�cy brak wody w zbiorniku

typedef enum {
	false, true
} bool; // boolowski typ danych

typedef struct {
	volatile uint8_t *KPIN;
	uint8_t key_mask;
	uint8_t wait_time_s;
	void (*kfun1)(void);
	void (*kfun2)(void);
	uint8_t klock;
	uint8_t flag;
} TBUTTON; // Struktura do obs�ugi przycisku

volatile bool water_error = false;  // flaga do sygnalizacji braku wody
volatile uint16_t Timer1;

TBUTTON button1;

void key_press(TBUTTON * btn); // funkcja do obs�ugi wci�ni�cia przycisku
void water(); // uruchomienie pompy
void my_delay(long int ms); // op�nienie

int main(void) {

	/* Ustawienie pin�w kontrolera */

	DDRB |= POMP;
	PORTB &= ~POMP;

	DDRB |= BUZZ;
	PORTB &= ~BUZZ;

	DDRB &= ~KEY1;
	PORTB |= KEY1;

	DDRB &= ~SENS;
	PORTB &= ~SENS;

	DDRB &= ~CHECK;
	PORTB |= CHECK;

	button1.KPIN = &PINB;
	button1.key_mask = KEY1;
	button1.wait_time_s = 5;
	button1.kfun1 = water;
	button1.kfun2 = 0;

	while (1) {

		key_press(&button1);

		if (!(PINB & CHECK)) {		// sprawdzenie stanu wody w zbiorniku
			if ((PINB & SENS)) { 	// sprawdzenie poziomu wilgotno�ci gleby
				PORTB |= POMP;		// uruchomienie pompy
				my_delay(1000);
				PORTB &= ~POMP;
				for (int i = 0; i < 250; i++) {
					my_delay(100);	// odczekanie odpowiedniej ilo�ci czasu na wsi�kni�cie wody
				}

			}
		} else {			// gdy nie ma wody w zbiorniku
			PORTB |= BUZZ;	// w��cz buzzer
			my_delay(200);
			PORTB &= ~BUZZ;
			for (int i = 0; i < 250; i++) {
				my_delay(300); // powiadomienie buzzerem co okre�lony czas
			}
		}
	}

}

void water() {
	PORTB |= POMP;
	_delay_ms(1000);
	PORTB &= ~POMP;
}

void key_press(TBUTTON * btn) {

	register uint8_t key_press = (*btn->KPIN & btn->key_mask);

	if (!btn->klock && !key_press) {
		btn->klock = 1;
		// reakcja na PRESS kr�tkie wcini�cie klawisza
		if (btn->kfun1)
			btn->kfun1();
		btn->flag = 1;
//		Timer1 = (btn->wait_time_s * 1000) / 10;
	} else if (btn->klock && key_press) {
		(btn->klock)++;
		if (!btn->klock) {
//			Timer1 = 0;
			btn->flag = 0;
		}
	} else if (btn->flag && !Timer1) {
		// reakcja na d�u�sze wcini�cie klawisza
		if (btn->kfun2)
			btn->kfun2();
		btn->flag = 0;
	}

}

void my_delay(long int ms) {
	for (int i = 0; i < ms; i++) {
		_delay_ms(1);
	}
}

